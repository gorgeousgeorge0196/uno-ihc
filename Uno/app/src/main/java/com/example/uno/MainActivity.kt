package com.example.uno

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.activity_main.*
import java.util.*
import kotlin.collections.ArrayList

class MainActivity : AppCompatActivity(), OnItemClickListener {


    var deck: ArrayList<Carta> = ArrayList()
    var mimano: ArrayList<Carta> = ArrayList()
    var oponenteArray: ArrayList<Carta> = ArrayList()
    var cartaCentro: Carta = Carta(0,0)



    override fun onItemClicked(carta: Carta, position: Int) {
        if(mimano[position].valor == cartaCentro.valor || mimano[position].color == cartaCentro.color){
            Log.d("acierto", "Funciona")
            mimano.removeAt(position)

            llenarAdapter()
            cartaCentro.valor = carta.valor
            cartaCentro.valor = carta.color
            fondo.background = ContextCompat.getDrawable(this,cartaCentro.color)
            txtjuego.text = cartaCentro.valor.toString()
        }else{
            Toast.makeText(this,"Toma una carta", Toast.LENGTH_SHORT).show()
        }
    }


    override fun onCreate(savedInstanceState: Bundle?) {


        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        for(vuelta in 0..1){
            for (colores in 0..3){
                for(valores in 0..14){
                    var fondoCarta: Int
                    when (colores){
                        0 -> {
                            fondoCarta = R.mipmap.uno_card_red
                        }
                        1 -> {
                            fondoCarta = R.mipmap.uno_card_blue
                        }
                        2 -> {
                            fondoCarta = R.mipmap.uno_card_green
                        }
                        3 -> {
                            fondoCarta = R.mipmap.uno_card_yellow
                        }
                        else -> {
                            fondoCarta = R.mipmap.back_side_card
                        }
                    }
                    var nuevaCarta: Carta = Carta(0, 0)
                    if (vuelta == 0){
                        nuevaCarta = Carta(fondoCarta, valores)
                        deck.add(nuevaCarta)
                        Log.d("Imprime arta", "agrego ${nuevaCarta.valor} ${nuevaCarta.color}")
                    }else{
                        if (valores != 0) {
                            nuevaCarta = Carta(fondoCarta, valores)
                            deck.add(nuevaCarta)
                            Log.d("Imprime arta", "agrego ${nuevaCarta.valor} ${nuevaCarta.color}")
                        }
                    }
                }
            }
        }

        //AGREGAR CARTA A LA MANO
        for (repartir in 0..6){
            val random = Random().nextInt(deck.size)
            Log.d("valor Random: ", "${random}")
            mimano.add(deck.get(random))
            deck.removeAt(random)
        }


        llenarCarta()

        llenarAdapter()

        llenaroponente()
        Handler().postDelayed({
            Toast.makeText(this,"Espera",Toast.LENGTH_SHORT).show()
        }, 2000)

btnrobar.setOnClickListener(){
    Toast.makeText(this,"Ya tomaste una carta",Toast.LENGTH_SHORT).show()
    if (deck.size != 0){
        val random = Random().nextInt(deck.size)
        mimano.add(deck.get(random))

    }else{
        Toast.makeText(this,"Fin del Juego",Toast.LENGTH_SHORT).show()
    }

    //validacion

    if(mimano[mimano.size-1].valor == cartaCentro.valor || mimano[mimano.size-1].color == cartaCentro.color){
        cartaCentro.valor = mimano[mimano.size-1].valor
        cartaCentro.color = mimano[mimano.size-1].color
        fondo.background = ContextCompat.getDrawable(this,cartaCentro.color)
        txtjuego.text = cartaCentro.valor.toString()
        mimano.removeAt(mimano.size-1)
    }
}


    }


    fun llenaroponente(){
        val oponenteadapter = OponenteAdapter(oponenteArray,this)

        oponente.adapter = oponenteadapter
        oponente.layoutManager = LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL,false)
    }


fun llenarAdapter(){
    val mimanoAdapter = MimanoAdapter(mimano,this,this)

    jugador.adapter = mimanoAdapter
    jugador.layoutManager = LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL,false)
}
    fun llenarCarta(){
        do{
            val random = Random().nextInt(deck.size)
            cartaCentro = deck.get(random)

            fondo.background = ContextCompat.getDrawable(this,cartaCentro.color)
            txtjuego.text = cartaCentro.valor.toString()
            deck.removeAt(random)
        }while (cartaCentro.valor >9)
    }


}