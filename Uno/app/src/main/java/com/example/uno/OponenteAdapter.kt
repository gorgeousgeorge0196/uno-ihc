package com.example.uno

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.carta_uno.view.*

class OponenteAdapter( private val mimanoArray: ArrayList<Carta>, val context: Context): RecyclerView.Adapter<OponenteAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): OponenteAdapter.ViewHolder {
        val cartaLayout = LayoutInflater.from(parent.context).inflate(R.layout.carta_uno,parent,false)
        return ViewHolder(cartaLayout)
    }
    override fun onBindViewHolder(holder: OponenteAdapter.ViewHolder, position: Int) {
        holder.fondoCard.background = ContextCompat.getDrawable(context, R.mipmap.back_side_card)
        holder.textoCentro.text = ""
    }


    override fun getItemCount() = mimanoArray.size

    class ViewHolder(view: View):RecyclerView.ViewHolder(view){
        val textoCentro =  view.texto_centro
        val fondoCard = view.fondo

    }

}