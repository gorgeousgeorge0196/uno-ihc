package com.example.uno

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.carta_uno.view.*

class MimanoAdapter ( private val mimanoArray: ArrayList<Carta>, val context: Context, val itemClickListener: OnItemClickListener): RecyclerView.Adapter<MimanoAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MimanoAdapter.ViewHolder {
        val cartaLayout =
            LayoutInflater.from(parent.context).inflate(R.layout.carta_uno, parent, false)
        return ViewHolder(cartaLayout)
    }

    override fun onBindViewHolder(holder: MimanoAdapter.ViewHolder, position: Int) {
        holder.fondoCard.background =
            ContextCompat.getDrawable(context, mimanoArray[position].color)
        holder.textoCentro.text = mimanoArray[position].valor.toString()

        holder.bind(mimanoArray[position], position, itemClickListener)

        //HOLDER . ONCLICK  PARA METODOS INDIVIDUALES DE LA CARTA
    }

    override fun getItemCount() = mimanoArray.size

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val textoCentro = view.texto_centro
        val fondoCard = view.fondo
        var cartaPos = 0

        fun bind(carta: Carta, position: Int, clickListener: OnItemClickListener) {
            itemView.texto_centro.text = carta.valor.toString()
            itemView.fondo.background = ContextCompat.getDrawable(itemView.context, carta.color)
            cartaPos = position
            itemView.setOnClickListener() {
                clickListener.onItemClicked(carta, position)
            }
        }
    }


}

interface OnItemClickListener {
    fun onItemClicked(carta: Carta, position: Int)
}